const { User } = require('../../../models/user');
const jwt = require('jsonwebtoken');
const config = require('config');
const mongoose = require('mongoose');

describe('generateAuthToken', () => {
    it('should return a valid auth token', async () => {
        const payload = { 
            _id: new mongoose.Types.ObjectId().toHexString(), 
            isAdmin: true 
        };
        const user = new User(payload);
        var token = user.generateAuthToken();

        var result = await jwt.verify(token, config.get('jwtPrivateKey'));

        expect(result).toMatchObject(payload);
    });
});