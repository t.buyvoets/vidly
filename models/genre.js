const Joi = require('joi');
const mongoose = require('mongoose');

const genreSchema = mongoose.Schema({
    genre: {
        type: String,
        required: true
    }
})

const Genre = mongoose.model('Genre', genreSchema);

function validateGenre(genre) {
    const schema = Joi.object({
        genre: Joi.string().required()
    });
    return schema.validate(genre);
}

exports.genreSchema = genreSchema;
exports.Genre = Genre;
exports.validate = validateGenre;