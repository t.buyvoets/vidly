const mongoose = require('mongoose');
const Joi = require('joi');

const Customer = mongoose.model('customer', mongoose.Schema({
    isGold: {
        type: Boolean,
        required: true 
    },
    name: {
        type: String,
        required: true,
        min: 3,
        max: 50 
    },
    phone: { 
        type: String
    }
}));

function validateCustomer(customer) {
    const schema = Joi.object({
        isGold: Joi.boolean().required(),
        name: Joi.string().required().min(3).max(50),
        phone: Joi.string().required()
    });
    return schema.validate(customer);
}

exports.Customer = Customer;
exports.validate = validateCustomer;