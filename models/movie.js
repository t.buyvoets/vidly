const Joi = require('joi');
const mongoose = require('mongoose');
const { genreSchema } = require('./genre');

const Movie = mongoose.model('Movie', mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    numberInStock: {
        type: Number,
        required: true,
        min: 0
    },
    dailyRentalRate: {
        type: Number,
        required: true,
        min: 0
    },
    genre: {
        type: genreSchema,
        required: true
    }
}));

function validateMovie(movie) {
    const schema = Joi.object({
        title: Joi.string().required(),
        numberInStock: Joi.number().required().min(0),
        dailyRentalRate: Joi.number().required().min(0),
        genreId: Joi.objectId().required()
    });
    return schema.validate(movie);
}

exports.Movie = Movie;
exports.validate = validateMovie;