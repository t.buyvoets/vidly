const mongoose = require('mongoose');
const winston = require('winston');

module.exports = function() {
    mongoose.connect('mongodb://192.168.60.20/vidly')
        .then(() => winston.info('Connected to MongoDB...'));
}

